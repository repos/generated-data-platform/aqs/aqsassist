package aqsassist

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/hhsnopek/etag"
)

var HeadersToAdd = map[string]string{
	"access-control-allow-headers":  "accept, content-type, content-length, cache-control, accept-language, api-user-agent, if-match, if-modified-since, if-none-match, dnt, accept-encoding",
	"access-control-allow-methods":  "GET,HEAD",
	"access-control-allow-origin":   "*",
	"access-control-expose-headers": "etag",
	"cache-control":                 "s-maxage=14400, max-age=14400",
	"referrer-policy":               "origin-when-cross-origin",
	"x-content-type-options":        "nosniff",
	"x-xss-protection":              "1; mode=block",
	"x-frame-options":               "deny",
	"content-type":                  "application/json; charset=utf-8",
	"content-security-policy":       "default-src 'none'; frame-ancestors 'none'",
}

var AgentValues = []string{"all-agents", "spider", "user"}
var MediaTypeValues = []string{"all-media-types", "image", "video", "audio", "document", "other"}
var GranularityValues = []string{"daily", "monthly"}
var ActivityLevelValues = []string{"all-activity-levels", "1..4-edits", "5..24-edits", "25..99-edits", "100..-edits"}
var PageTypeValues = []string{"all-page-types", "content", "non-content"}
var EditorTypeValues = []string{"all-editor-types", "anonymous", "group-bot", "name-bot", "user"}
var AccessSitesValues = []string{"all-sites", "desktop-site", "mobile-site"}
var refererValues = []string{"internal", "external", "search-engine", "unknown", "none"}
var EditValues = []string{"create", "update", "all-edit-types"}
var CategoryScopeValues = []string{"shallow", "deep"}

const DatePattern = "2006010215"

func ValidateTimestamp(param string) (string, error) {
	var err error
	var timestamp string

	// We accept timestamp parameters of two forms, YYYYMMDD and YYYYMMDDHH.
	// If timestamp parameter is 8 bytes length (8 ASCII runes),
	// then suffix the string with "00".

	if len(param) == 8 {
		timestamp = fmt.Sprintf("%s00", param)
	} else {
		timestamp = param
	}

	if _, err = time.Parse(DatePattern, timestamp); err != nil {
		return "", err
	}

	return timestamp, nil
}

// TrimProjectDomain Normalize a project name (lower case and removing ".org" suffix and "www." prefix)
func TrimProjectDomain(param string) string {
	return strings.TrimPrefix(strings.TrimSuffix(strings.ToLower(param), ".org"), "www.")
}

// ValidateProject Validate and normalize a project
func ValidateProject(project string) (bool, string) {
	project = TrimProjectDomain(project)
	matches, _ := regexp.MatchString("^[a-z0-9_.\\-]+$", project)
	return matches, project
}

// ValidateReferer Validate and normalize a referer
func ValidateReferer(referer string) (bool, string) {
	referer = strings.ToLower(referer)
	isValidReferer := isReferer(referer)
	// If it's not a valid referer, it must be validated as a project one
	if !isValidReferer {
		isValidProject, referer := ValidateProject(referer)
		return isValidProject, referer
	}

	return isValidReferer, referer
}

// NormalizePageTitle Normalize a page title, replacing spaces with underscores
func NormalizePageTitle(pageTitle string) string {
	return strings.ReplaceAll(pageTitle, " ", "_")
}

func SetSecurityHeaders(w http.ResponseWriter, body []byte) {
	for headerName, headerValue := range HeadersToAdd {
		w.Header().Set(headerName, headerValue)
	}
	w.Header().Set("Etag", etag.Generate(body, true))
}

func FilterAgent(agent string, spider int, user int) (int, error) {
	if agent == "user" {
		return user, nil
	} else if agent == "spider" {
		return spider, nil
	} else if agent == "all-agents" {
		return spider + user, nil
	} else {
		return 0, errors.New("invalid agent")
	}
}

func ValidateDuration(start string, end string, granularity string) (string, string, error) {
	timeStart, _ := time.Parse(DatePattern, start)
	timeEnd, _ := time.Parse(DatePattern, end)

	if granularity == "monthly" {
		if timeEnd.After(timeStart.AddDate(0, 2, 0)) {
			return start, end, nil
		} else if timeEnd.After(timeStart.AddDate(0, 1, 0)) {
			if timeStart.Day() != 01 && timeEnd.Add(time.Hour*24).Day() != 01 {
				return "", "", errors.New("no full months found in specified date range")
			}
			return start, end, nil
		} else {
			if timeStart.Day() != 01 || timeEnd.Add(time.Hour*24).Day() != 01 {
				return "", "", errors.New("no full months found in specified date range")
			}
			return start, end, nil
		}
	} else {
		return start, end, nil
	}
}

func StartBeforeEnd(start string, end string) error {
	timeStart, e := time.Parse(DatePattern, start)
	if e != nil {
		return errors.New("start timestamp is invalid, must be a valid date in YYYYMMDD format")
	}
	timeEnd, e := time.Parse(DatePattern, end)
	if e != nil {
		return errors.New("end timestamp is invalid, must be a valid date in YYYYMMDD format")
	}

	if timeStart.After(timeEnd) {
		return errors.New("start timestamp should be before the end timestamp")
	} else {
		return nil
	}
}

// GetTime Returns a date, as a time.Time object, given year, month and day as string values
func GetTime(year, month, day string) time.Time {
	yearValue, _ := strconv.Atoi(year)
	monthValue, _ := strconv.Atoi(month)
	dayValue := 0
	if day == "all-days" {
		dayValue = 1
	} else {
		dayValue, _ = strconv.Atoi(day)
	}

	return time.Date(yearValue, time.Month(monthValue), dayValue, 0, 0, 0, 0, time.UTC)
}

// GetMarshallTextTime Returns a parsed date, as a string according to RFC 3339, given a year, month and day as string values
func GetMarshallTextTime(year, month, day string) string {
	stringDate, _ := GetTime(year, month, day).MarshalText()
	return string(stringDate)
}

// GetStartEndDate Returns start and end dates, as time.Time objects, for a given day of month (or the whole month if "all-days" string is provided as the day value)
func GetStartEndDate(year, month, day string) (time.Time, time.Time) {
	var startDate time.Time
	var endDate time.Time
	if day == "all-days" {
		startDate = GetTime(year, month, "1")
		endDate = startDate.AddDate(0, 1, 0)
	} else {
		startDate = GetTime(year, month, day)
		endDate = startDate.AddDate(0, 0, 1)
	}

	return startDate, endDate
}

// ConvertToYYYYMMDDHH converts a time.Time object to a string in the format YYYYMMDDHH
func ConvertToYYYYMMDDHH(t time.Time) string {
	return t.Format("2006010215")
}

// IsYear Validates whether a year is valid
func IsYear(year string) bool {
	return govalidator.IsInt(year) && len(year) == 4
}

// IsMonth Validates whether a month is valid
func IsMonth(month string) bool {
	if len(month) != 2 {
		return false
	}
	if !govalidator.IsInt(strings.TrimLeft(month, "0")) {
		return false
	}
	monthValue, _ := strconv.Atoi(month)
	return monthValue >= 1 && monthValue <= 12
}

// IsDay Validates whether a day is valid for a specific month and year (a two-digits integer or "all-days" string when supported)
func IsDay(year, month, day string, allDaysSupport bool) bool {
	if allDaysSupport && day == "all-days" {
		return true
	}

	if !govalidator.IsInt(strings.TrimLeft(day, "0")) {
		return false
	}

	if len(day) != 2 {
		return false
	}

	dayValue, _ := strconv.Atoi(day)
	return dayValue >= 1 && dayValue <= GetDaysIn(year, month)
}

// GetDaysIn Returns which is the last day for a specific month and year
func GetDaysIn(year, month string) int {
	monthValue, _ := strconv.Atoi(month)
	yearValue, _ := strconv.Atoi(year)
	return time.Date(yearValue, time.Month(monthValue)+1, 0, 0, 0, 0, 0, time.UTC).Day()
}

// ValidateFullMonthsBetween Validate dates (and adjust them when available) to ensure dates range include only full months
func ValidateFullMonthsBetween(start, end string, untilFirstDayNextMonth bool) (string, string, error) {
	// Format start/end dates to YYYYMMDDHH if needed
	if len(start) == 8 {
		start = fmt.Sprintf("%s00", start)
	}
	if len(end) == 8 {
		end = fmt.Sprintf("%s00", end)
	}

	startDate, _ := time.Parse(DatePattern, start)
	endDate, _ := time.Parse(DatePattern, end)

	// Some services consider the last day of the month as valid date. Others the first day of the next month
	if untilFirstDayNextMonth {
		if endDate.Day() != 1 {
			endDate = time.Date(endDate.Year(), endDate.Month(), 1, 0, 0, 0, 0, time.UTC)
		}
	} else {
		if endDate.Day() < GetDaysIn(strconv.Itoa(endDate.Year()), strconv.Itoa(int(endDate.Month()))) {
			monthValue := int(endDate.Month()) - 1
			endDate = time.Date(endDate.Year(), time.Month(monthValue), GetDaysIn(strconv.Itoa(endDate.Year()), strconv.Itoa(monthValue)),
				0, 0, 0, 0, time.UTC)
		}
	}

	// Only when start begins with "01" we can consider it as a full month
	if startDate.Day() > 1 {
		monthValue := int(startDate.Month())
		startDate = time.Date(startDate.Year(), time.Month(monthValue+1), 1, 0, 0, 0, 0, time.UTC)
	}

	// Check for the difference between start and date (it must be, at least, one month)
	differenceInDays := int(endDate.Sub(startDate).Hours() / 24)
	monthValue := strconv.Itoa(int(startDate.Month()))
	if untilFirstDayNextMonth {
		if differenceInDays < GetDaysIn(strconv.Itoa(startDate.Year()), monthValue) {
			return start, end, errors.New("no full months between dates")
		}
	} else {
		if differenceInDays < GetDaysIn(strconv.Itoa(startDate.Year()), monthValue)-1 {
			return start, end, errors.New("no full months between dates")
		}
	}

	return startDate.Format(DatePattern), endDate.Format(DatePattern), nil
}

// contains Checks whether the given value is present in the given slice
func contains(validValues []string, givenValue string) bool {
	for _, value := range validValues {
		if value == givenValue {
			return true
		}
	}

	return false
}

// IsEditorType Checks whether editorType has a valid value
func IsEditorType(editorType string) bool {
	return contains(EditorTypeValues, editorType)
}

// IsPageType Checks whether pageType has a valid value
func IsPageType(pageType string) bool {
	return contains(PageTypeValues, pageType)
}

// IsActivityLevel Checks whether activityLevel has a valid value
func IsActivityLevel(activityLevel string) bool {
	return contains(ActivityLevelValues, activityLevel)
}

// IsGranularity Checks whether granularity has a valid value
func IsGranularity(granularity string) bool {
	return contains(GranularityValues, granularity)
}

// IsMediaType Checks whether mediaType has a valid value
func IsMediaType(mediaType string) bool {
	return contains(MediaTypeValues, mediaType)
}

// IsAgent Checks whether agent has a valid value
func IsAgent(agent string) bool {
	return contains(AgentValues, agent)
}

// IsAccessSite Checks whether access has a valid value
func IsAccessSite(accessSite string) bool {
	return contains(AccessSitesValues, accessSite)
}

func isReferer(referer string) bool {
	return contains(refererValues, referer)
}

func ValidateYearMonth(year string, month string) (string, string, error) {
	if !IsYear(year) || !IsMonth(month) {
		return "", "", errors.New(YearMonthInvalidDateMessage)
	}
	return year, month, nil
}

func IsEditValue(value string) bool {
	return contains(EditValues, value)
}

func IsCategoryScope(value string) bool {
	return contains(CategoryScopeValues, value)
}
