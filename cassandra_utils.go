package aqsassist

import (
	"fmt"
	"strings"

	"github.com/gocql/gocql"
)

// Return a new Cassandra session corresponding to the provided config.
func NewCassandraSession(config *Config) (*gocql.Session, error) {
	var cluster *gocql.ClusterConfig = gocql.NewCluster(config.Cassandra.Hosts...)

	cluster.Consistency, _ = GoCQLConsistency(config.Cassandra.Consistency)
	cluster.Port = config.Cassandra.Port

	// Host selection
	if config.Cassandra.LocalDC != "" {
		cluster.PoolConfig.HostSelectionPolicy = gocql.DCAwareRoundRobinPolicy(config.Cassandra.LocalDC)
	} else {
		cluster.PoolConfig.HostSelectionPolicy = gocql.RoundRobinHostPolicy()
	}

	// TLS
	tlsConf := config.Cassandra.TLS
	if tlsConf.CaPath != "" {
		cluster.SslOpts = &gocql.SslOptions{
			CaPath: tlsConf.CaPath,
		}
		cluster.SslOpts.CertPath = tlsConf.CertPath
		cluster.SslOpts.KeyPath = tlsConf.KeyPath
	}

	// Authentication
	authConf := config.Cassandra.Authentication
	if authConf.Username != "" {
		cluster.Authenticator = gocql.PasswordAuthenticator{
			Username: authConf.Username,
			Password: authConf.Password,
		}
	}

	return cluster.CreateSession()
}

// Given a string, return the corresponding GoCQL consistency level type.
func GoCQLConsistency(c string) (gocql.Consistency, error) {
	switch strings.ToLower(c) {
	case "any":
		return gocql.Any, nil
	case "one":
		return gocql.One, nil
	case "two":
		return gocql.Two, nil
	case "three":
		return gocql.Three, nil
	case "quorum":
		return gocql.Quorum, nil
	case "all":
		return gocql.All, nil
	case "localquorum":
		return gocql.LocalQuorum, nil
	case "eachquorum":
		return gocql.EachQuorum, nil
	case "localone":
		return gocql.LocalOne, nil
	default:
		return 0, fmt.Errorf("Unrecognized Cassandra consistency level")
	}
}
