/*
 * Copyright 2022 Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aqsassist

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gocql/gocql"
	yaml "gopkg.in/yaml.v2"
)

// Config represents an application-wide configuration.
type Config struct {
	ServiceName    string          `yaml:"service_name"`
	Address        string          `yaml:"listen_address"`
	Port           int             `yaml:"listen_port"`
	LogLevel       string          `yaml:"log_level"`
	ContextTimeout int             `yaml:"context_timeout"`
	Cassandra      CassandraConfig `yaml:"cassandra"`
	Druid          DruidConfig     `yaml:"druid"`
}

// CassandraConfig represents a Cassandra configuration.
type CassandraConfig struct {
	Port        int      `yaml:"port"`
	Consistency string   `yaml:"consistency"`
	Hosts       []string `yaml:"hosts"`
	LocalDC     string   `yaml:"local_dc"`
	ConfigTable string   `yaml:"config_table"`

	TLS struct {
		CaPath   string `yaml:"ca"`
		CertPath string `yaml:"cert"`
		KeyPath  string `yaml:"key"`
	}
	Authentication struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
	}
}

// DruidConfig represents a Druid configuration.
type DruidConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`

	TLS struct {
		CaPath   string `yaml:"ca"`
		CertPath string `yaml:"cert"`
		KeyPath  string `yaml:"key"`
	}
	Authentication struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
	}
}

// NewConfig returns a new Config from YAML serialized as bytes.
func NewConfig(data []byte) (*Config, error) {
	// Populate a new Config with sane defaults.
	config := Config{
		ServiceName:    "service-scaffold-golang",
		Address:        "localhost",
		Port:           8080,
		LogLevel:       "info",
		ContextTimeout: 40,
		Cassandra: CassandraConfig{
			Port:        9042,
			Consistency: "quorum",
			Hosts:       []string{"localhost"},
			ConfigTable: "aqs.config",
		},
		Druid: DruidConfig{
			Host: "http://localhost",
			Port: "8082",
		},
	}
	// Override the defaults with the given data.
	err := yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}
	return Validate(&config)
}

// ReadConfig returns a new Config object from a YAML input file.
func ReadConfig(filename string) (*Config, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return NewConfig(data)
}

// Validate ensures all configuration properties are correct.
func Validate(config *Config) (*Config, error) {
	if err := ValidateCassandraConsistency(config.Cassandra); err != nil {
		return nil, err
	}
	if err := ValidateLogLevel(config.LogLevel); err != nil {
		return nil, err
	}
	return config, nil
}

// ValidateCassandraConsistency ensures a valid cassandra consistency.
func ValidateCassandraConsistency(c CassandraConfig) error {
	switch strings.ToLower(c.Consistency) {
	case "any", "one", "two", "three", "quorum", "all", "localquorum", "eachquorum", "localone":
		return nil
	}
	return fmt.Errorf(UnsupportedConsistencyErrorMessage, c.Consistency)
}

// ValidateLogLevel ensures a valid log level.
func ValidateLogLevel(logLevel string) error {
	switch strings.ToUpper(logLevel) {
	case "DEBUG", "INFO", "WARNING", "ERROR", "FATAL":
		return nil
	}

	return fmt.Errorf(UnsupportedLogLevelMessage, logLevel)
}

// DynamicConfig represents the configuration pulled from
// a remote config store which can be updated dinamically.
type DynamicConfig struct {
	Reader DynamicConfigReaderInterface
	TTL    int64
	Cache  map[string]ValueAndExpiration
}

// DynamicConfigReaderInterface represents a source of configuration values
// for a DynamicConfig object. It needs to implement a Get method.
type DynamicConfigReaderInterface interface {
	Get(property string) (string, error)
}

// ValueAndExpiration captures a cached config value and its expiration time.
// The expiration is a unix timestamp (seconds elapsed since unix epoch).
type ValueAndExpiration struct {
	Value      string
	Expiration int64
}

// NewDynamicConfig creates a new instance of DynamicConfig with an empty cache.
func NewDynamicConfig(reader DynamicConfigReaderInterface, ttl int64) (*DynamicConfig, error) {
	return &DynamicConfig{
		Reader: reader,
		TTL:    ttl,
		Cache:  map[string]ValueAndExpiration{},
	}, nil
}

// GetString returns the value of the given property as a string.
// If the property is cached and has not expired, returns the cached value.
// Otherwise, the property is fetched from the source and returned.
func (dc DynamicConfig) GetString(property string) (string, error) {
	now := time.Now()
	cached, ok := dc.Cache[property]
	if ok && cached.Expiration > now.Unix() {
		return cached.Value, nil
	} else {
		value, err := dc.Reader.Get(property)
		if err != nil {
			return "", err
		}
		// We need to cast the TTL into a Duration to be able to multiply.
		expiration := now.Add(time.Second * time.Duration(dc.TTL)).Unix()
		dc.Cache[property] = ValueAndExpiration{value, expiration}
		return value, nil
	}
}

// CassandraConfigReader implements DynamicConfigReaderInterface and represents
// the current source of configuration values for DynamicConfig objects.
// When we develop a dataset state store, we'll use that instead.
type CassandraConfigReader struct {
	CassandraSession *gocql.Session
	CassandraTable   string
}

// Get gets the given property value from the given cassandra table.
func (cr CassandraConfigReader) Get(property string) (string, error) {
	var value string
	query := fmt.Sprintf("SELECT value FROM %s WHERE param = ?", cr.CassandraTable)
	err := cr.CassandraSession.Query(query, property).Scan(&value)
	return value, err
}
