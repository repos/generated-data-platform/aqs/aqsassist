package aqsassist

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"gopkg.in/yaml.v3"

	"github.com/grafadruid/go-druid"
	"github.com/grafadruid/go-druid/builder"
	"github.com/grafadruid/go-druid/builder/aggregation"
	"github.com/grafadruid/go-druid/builder/datasource"
	"github.com/grafadruid/go-druid/builder/dimension"
	"github.com/grafadruid/go-druid/builder/filter"
	"github.com/grafadruid/go-druid/builder/granularity"
	"github.com/grafadruid/go-druid/builder/intervals"
	"github.com/grafadruid/go-druid/builder/query"
	"github.com/grafadruid/go-druid/builder/topnmetric"
	"github.com/grafadruid/go-druid/builder/types"
)

// DruidUtils store useful fields/values to use when building Druid queries using the builder pattern
type DruidUtils struct {
	Dimension     Dimension     `yaml:"dimension"`
	Metric        Metric        `yaml:"metric"`
	EventEntity   EventEntity   `yaml:"event_entity"`
	EventType     EventType     `yaml:"event_type"`
	UserType      UserType      `yaml:"user_type"`
	PageType      PageType      `yaml:"page_type"`
	OtherTags     OtherTags     `yaml:"other_tags"`
	Value         Value         `yaml:"value"`
	Aggregated    Aggregated    `yaml:"aggregated"`
	ProjectFamily ProjectFamily `yaml:"project_family"`
	TopThreshold  int64         `yaml:"top_threshold"`
}

// Dimension Druid dimensions we use to query
// All the needed Druid dimensions (column names) to build the queries
// Some dimensions (event_entity, event_type, user_type, page_type and other_tags) have a ranged set of values that are defined below
type Dimension struct {
	EventEntity   string `yaml:"event_entity"`
	EventType     string `yaml:"event_type"`
	Project       string `yaml:"project"`
	UserId        string `yaml:"user_id"`
	UserText      string `yaml:"user_text"`
	UserType      string `yaml:"user_type"`
	PageId        string `yaml:"page_id"`
	PageTitle     string `yaml:"page_title"`
	PageNamespace string `yaml:"page_namespace"`
	PageType      string `yaml:"page_type"`
	OtherTags     string `yaml:"other_tags"`
	Revisions     string `yaml:"revisions"`
}

// EventEntity represent the type of entity related to an event (a revision, page or user)
type EventEntity struct {
	Revision string `yaml:"revision"`
	Page     string `yaml:"page"`
	User     string `yaml:"user"`
}

// EventType Represent the type of generated events
type EventType struct {
	Create        string `yaml:"create"`
	Delete        string `yaml:"delete"`
	Restore       string `yaml:"restore"`
	DailyDigest   string `yaml:"daily_digest"`
	MonthlyDigest string `yaml:"monthly_digest"`
}

// UserType represent the different user types
type UserType struct {
	Anonymous string `yaml:"anonymous"`
	GroupBot  string `yaml:"group_bot"`
	NameBot   string `yaml:"name_bot"`
	User      string `yaml:"user"`
}

// PageType represent the content type of the page affected by an event
type PageType struct {
	Content    string `yaml:"content"`
	NonContent string `yaml:"non_content"`
}

// OtherTags represent some other information about the event
type OtherTags struct {
	Redirect    string `yaml:"redirect"`
	Deleted     string `yaml:"deleted"`
	SelfCreated string `yaml:"self_created"`
}

// Metric Some fields used to calculate aggregated results in some specific endpoints
type Metric struct {
	Events              string `yaml:"events"`
	TextBytesDiffSum    string `yaml:"text_bytes_diff_sum"`
	TextBytesDiffAbsSum string `yaml:"text_bytes_diff_abs_sum"`
}

// Value Some key values to fill when preparing queries
// Some useful keywords to use as values when building queries from the specific request
// Some of them come in the request (AllActivityLevels, AllEditorTypes, AllPageTypes, AllDays, Daily and Monthly) to represent a special value for a parameter
// The others (Digest and All) are just some values to use as keywords somewhere when building the queries
type Value struct {
	AllActivityLevels string `yaml:"all_activity_levels"`
	AllEditorTypes    string `yaml:"all_editor_types"`
	AllPageTypes      string `yaml:"all_page_types"`
	AllDays           string `yaml:"all_days"`
	Daily             string `yaml:"daily"`
	Monthly           string `yaml:"monthly"`
	Digest            string `yaml:"digest"`
	All               string `yaml:"all"`
}

// Aggregated Some keywords to use when aggregation is needed
type Aggregated struct {
	Count             string `yaml:"count"` // used for COUNT aggregations (see DruidQueryParams.AggregationType)
	Sum               string `yaml:"sum"`   // used for SUM aggregations (see DruidQueryParams.AggregationType)
	Result            string `yaml:"result"`
	Timestamp         string `yaml:"timestamp"`
	AggregationResult string `yaml:"aggregation_result"`
}

// ProjectFamily regex to use when looking for family projects in some specific endpoints that allow this
type ProjectFamily struct {
	AllWikipediaProjects   string `yaml:"all_wikipedia_projects"`
	AllWikivoyageProjects  string `yaml:"all_wikivoyage_projects"`
	AllWiktionaryProjects  string `yaml:"all_wiktionary_projects"`
	AllWikibooksProjects   string `yaml:"all_wikibooks_projects"`
	AllWikinewsProjects    string `yaml:"all_wikinews_projects"`
	AllWikiquoteProjects   string `yaml:"all_wikiquote_projects"`
	AllWikisourceProjects  string `yaml:"all_wikisource_projects"`
	AllWikiversityProjects string `yaml:"all_wikiversity_projects"`
}

// DruidQueryParams Structure that stores the query params to be passed to the specific Druid query function (ProcessTimeseriesQuery or ProcessTimeseriesQuery)
// Just the necessary values for each query are required. For example: sometimes Year/Month/Day will be needed to specify date and, some others, start/end will be needed parameters
// Project, EditorType, PageType, PageTitle, ActivityLevel, Granularity, Year, Month, Day, Start and End must be filled using the request parameters values
// EventEntity, EventType, NotOtherTags, OtherTags, AggregationType, AggregationField and TopDimension must be filled using Druid Schema values (see DruidUtils structure)
type DruidQueryParams struct {
	Project          string // project name value that comes in the request (or keywords like 'all-projects' or family project regex)
	EditorType       string // editor type value that comes in the request
	PageType         string // page type value that comes in the request
	PageTitle        string // page title value that comes in the request
	ActivityLevel    string // activity level value that comes in the request
	Granularity      string // granularity value that comes in the request
	Year             string // Year, Month, Day values specified in the request, when a specific date is needed
	Month            string
	Day              string
	Start            string // Start, End values specified in the request, when a date range is needed
	End              string
	EventEntity      string // event entity. Its value depends on the needed query (see EventEntity structure)
	EventType        string // event type. Its value depends on the needed query (see EventType structure)
	NotOtherTags     string // always used as a NOT filter with other_tags field (see OtherTags structure)
	OtherTags        string // always used as a EQUAL filter with other_tags field (see OtherTags structure)
	AggregationType  string // Type of aggregation when needed: count or sum (see Aggregated structure)
	AggregationField string // Field that will be aggregated (see Metric structure)
	TopDimension     string // Name of the field that store the aggregated value (see Dimension structure)
}

// ResultsTuple Store temporary data queried from DruidUtils to add it to the response object in the data layer
// When Rank is needed, it's added directly in the data layer as a counter
type ResultsTuple struct {
	Item        string
	Aggregation int
}

// Druid Structure to load all the Druid schema values
var Druid DruidUtils

// ReadDruidConfig Load druid-schemas.yaml file into a DruidUtils structure
func ReadDruidConfig(filename string) error {
	data, err := os.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("error loading druid schemas config file: %s", err.Error())
	}

	err = yaml.Unmarshal(data, &Druid)
	if err != nil {
		return fmt.Errorf("error while parsing schemas config file")
	}

	return nil
}

// getDruidBuilderGranularity Given an expression (monthly, all-days, daily, . . ) the right granularity is returned as a Druid Simple object
func getDruidBuilderGranularity(value string) *granularity.Simple {
	switch value {
	case Druid.Value.Monthly:
		return granularity.NewSimple().SetGranularity(granularity.Month)
	case Druid.Value.AllDays:
		return granularity.NewSimple().SetGranularity(granularity.Month)
	case Druid.Value.Daily:
		return granularity.NewSimple().SetGranularity(granularity.Day)
	default:
		return granularity.NewSimple().SetGranularity(granularity.Day)
	}
}

// GetGranularity Given the expression 'all-days' or a day number, right granularity is returned as a string
func GetGranularity(days string) string {
	switch days {
	case Druid.Value.AllDays:
		return Druid.Value.Monthly
	default:
		return Druid.Value.Daily
	}
}

// GetEventType Return the right type of event given a granularity ("daily" or "monthly")
func GetEventType(granularity string) string {
	if granularity == Druid.Value.Daily {
		return Druid.EventType.DailyDigest
	}

	return Druid.EventType.MonthlyDigest
}

// getDruidDate Parse a string timestamp to a DruidUtils-format string date ("2022-12-25")
func getDruidDate(timestamp string) string {
	return timestamp[:4] + "-" + timestamp[4:6] + "-" + timestamp[6:8]
}

// druidSeparator Change word-separator that comes from the path-params to use in DruidUtils (e.g.: from "non-content" to "non_content")
// It's useful because request params use dash "-" as the word separator but Druid dataset use the underscore "_" to do that
func druidSeparator(value string) string {
	return strings.Replace(value, "-", "_", 1)
}

// fromKebabCaseToUpperCamelCase Transform a kebab-case string to a UpperCamelCase one. Useful to find the right regex inside the Druid config structure
func fromKebabCaseToUpperCamelCase(value string) string {
	newValue := strings.Replace(value, "-", " ", -1)
	newValue = cases.Title(language.English).String(newValue)
	newValue = strings.Replace(newValue, " ", "", -1)

	return newValue
}

// getMinMaxActivityLevels Get min and max values for any activity-level keyword
// E.g.: When the activity-level request param can contain a value like "4..25-edits". When passing that value to this function, it returns both values: 1 and 25 as integer variables
func getMinMaxActivityLevels(activityLevel string) (int, int) {
	switch activityLevel {
	case Druid.Value.AllActivityLevels:
		return -1, -1
	case "100..-edits":
		return 100, -1
	default: // min..max values
		minMaxActivityLevel := strings.Split(activityLevel, "-")[0]
		minValue, _ := strconv.Atoi(strings.Split(minMaxActivityLevel, "..")[0])
		maxValue, _ := strconv.Atoi(strings.Split(minMaxActivityLevel, "..")[1])
		return minValue, maxValue
	}
}

// appendSelectorFilter Add a selector filter to a Filter array and returns it
func appendSelectorFilter(filtersBuilder []builder.Filter, filterField, filterValue string) []builder.Filter {
	return append(filtersBuilder, filter.NewSelector().SetDimension(filterField).SetValue(filterValue))
}

// appendExpressionFilter Add a selector filter to a Filter array and returns it
func appendExpressionFilter(filtersBuilder []builder.Filter, filterField, filterPattern string) []builder.Filter {
	return append(filtersBuilder, filter.NewRegex().SetDimension(filterField).SetPattern(filterPattern))
}

// getStringValueFromMap Extract an specific string value from a map structure (result structure)
func getStringValueFromMap(item interface{}, fieldName string) string {
	return fmt.Sprintf("%s", item.(map[string]interface{})[fieldName])
}

// getFloatValueFromMap Extract an specific float value from a map structure (result structure)
func getFloatValueFromMap(item interface{}, fieldName string) (float64, error) {
	return strconv.ParseFloat(fmt.Sprintf("%f", item.(map[string]interface{})[fieldName]), 0)
}

// getEventInformation Extract the event information from the result structure
func getEventInformation(item interface{}) interface{} {
	return item.(map[string]interface{})[Druid.Aggregated.Result]
}

// getItemsFromTopNQuery Get top items from the result structure (to use later to compose the final response)
func getItemsFromTopNQuery(result []interface{}) []interface{} {
	return result[0].(map[string]interface{})[Druid.Aggregated.Result].([]interface{})
}

// getDruidFiltersFromParameters Add all the filters that a query needs based on the queryParams structure
func getDruidFiltersFromParameters(queryParams DruidQueryParams) *filter.And {
	// Applying filters
	var filtersBuilder []builder.Filter
	// Project filters
	if !strings.HasPrefix(queryParams.Project, "all-") { // a specific project has been requested (no 'all' keyword)
		filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.Project, queryParams.Project)
	} else if queryParams.Project != "all-projects" { // a specific project family has been requested ('all-wikipedia-projects', for example)
		projectFamily := fromKebabCaseToUpperCamelCase(queryParams.Project)
		regexValue := reflect.ValueOf(Druid.ProjectFamily).FieldByName(projectFamily).String()
		filtersBuilder = appendExpressionFilter(filtersBuilder, Druid.Dimension.Project, regexValue)
	} // else == 'all-projects' -> No filter needed (data is not aggregated by project, so it will fetch data about all projects)
	// Editor Type filter
	if queryParams.EditorType != "" {
		if queryParams.EditorType == Druid.Value.AllEditorTypes && strings.Contains(queryParams.EventType, Druid.Value.Digest) {
			// 'all-editor-types' has been passed as a request parameter
			filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.UserType, Druid.Value.All)
		} else if queryParams.EditorType != Druid.Value.AllEditorTypes {
			// a specific editor type has been passed as a request parameter
			queryParams.EditorType = druidSeparator(queryParams.EditorType)
			filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.UserType, queryParams.EditorType)
		}
	}
	// Page Type filter
	if queryParams.PageType != "" {
		if queryParams.PageType == Druid.Value.AllPageTypes && strings.Contains(queryParams.EventType, Druid.Value.Digest) {
			// 'all-page-types' has been passed as a request parameter
			filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.PageType, Druid.Value.All)
		} else if queryParams.PageType != Druid.Value.AllPageTypes {
			// a specific page type has been passed as a request parameter
			queryParams.PageType = druidSeparator(queryParams.PageType)
			filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.PageType, queryParams.PageType)
		}
	}
	// Page Title filter
	if queryParams.PageTitle != "" {
		filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.PageTitle, queryParams.PageTitle)
	}
	// Activity Level filter (it filters always by "revisions" field)
	// No filter is needed in case 'all-activity-levels' value has been passed
	if queryParams.ActivityLevel != "" && queryParams.ActivityLevel != Druid.Value.AllActivityLevels {
		minActivityLevel, maxActivityLevel := getMinMaxActivityLevels(queryParams.ActivityLevel)
		boundFilter := filter.NewBound().SetDimension(Druid.Dimension.Revisions).SetOrdering(types.Numeric)
		if maxActivityLevel == -1 {
			boundFilter.SetLower(strconv.Itoa(minActivityLevel))
		} else {
			boundFilter.SetLower(strconv.Itoa(minActivityLevel))
			boundFilter.SetUpper(strconv.Itoa(maxActivityLevel))
		}
		filtersBuilder = append(filtersBuilder, boundFilter)
	}
	// Event entity/type filter
	filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.EventEntity, queryParams.EventEntity)
	if queryParams.EventType != "" {
		filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.EventType, queryParams.EventType)
	}
	// other_tags filter
	// In this case the query will include that value
	if queryParams.OtherTags != "" {
		filtersBuilder = appendSelectorFilter(filtersBuilder, Druid.Dimension.OtherTags, queryParams.OtherTags)
	}
	// Not other_tags filter
	// In this case the query will not include that value
	if queryParams.NotOtherTags != "" {
		notRedirectFilter := filter.NewSelector().SetDimension(Druid.Dimension.OtherTags).SetValue(queryParams.NotOtherTags)
		filtersBuilder = append(filtersBuilder, filter.NewNot().SetField(notRedirectFilter))
	}
	allFilters := filter.NewAnd().SetFields(filtersBuilder)

	return allFilters
}

// ProcessTimeseriesQuery Run a Timeseries query based on the query params, the datasource name and the current Druid connection.
// As a result it returns a ResultsTuple structure with all the data we need to compose the final response in the logic layer
func ProcessTimeseriesQuery(parameters DruidQueryParams, datasourceName string, druidClient *druid.Client) ([]ResultsTuple, error) {
	// Parameters customization (request context -> DruidUtils context)
	table := datasource.NewTable().SetName(datasourceName)
	queryGranularity := getDruidBuilderGranularity(parameters.Granularity)
	startDate := getDruidDate(parameters.Start)
	endDate := getDruidDate(parameters.End)
	interval := intervals.NewInterval().SetIntervalWithString(startDate, endDate)
	myIntervals := intervals.NewIntervals().SetIntervals([]*intervals.Interval{interval})

	// Collect all needed Druid filters based on the parameters that haven been passed in the request
	allFilters := getDruidFiltersFromParameters(parameters)

	// Prepare the aggregations (count or sum)
	var countOrSumAggregation []builder.Aggregator
	if parameters.AggregationType == Druid.Aggregated.Sum {
		countOrSumAggregation = []builder.Aggregator{aggregation.NewLongSum().
			SetName(Druid.Aggregated.AggregationResult).
			SetFieldName(parameters.AggregationField)}
	} else {
		countOrSumAggregation = []builder.Aggregator{aggregation.NewCount().SetName(Druid.Aggregated.AggregationResult)}
	}

	// Query execution
	scan := query.NewTimeseries().SetDataSource(table).
		SetIntervals(myIntervals).
		SetFilter(allFilters).
		SetAggregations(countOrSumAggregation).
		SetGranularity(queryGranularity).
		SetContext(map[string]interface{}{
			"skipEmptyBuckets": true,
		})

	// Get the query results
	var queryResults interface{}
	_, err := druidClient.Query().Execute(scan, &queryResults)
	if err != nil {
		return []ResultsTuple{}, err
	}

	// In case there is no data for the executed query, an empty ResultsTuple will be returned
	items := queryResults.([]interface{})
	if len(items) == 0 {
		return []ResultsTuple{}, nil
	}

	var results []ResultsTuple
	// If any data has been fetched, preprocess those results before returning them to data layer
	for _, item := range items {
		timestamp := getStringValueFromMap(item, Druid.Aggregated.Timestamp)
		eventInfo := getEventInformation(item)
		aggregationValue, _ := getFloatValueFromMap(eventInfo, Druid.Aggregated.AggregationResult)
		results = append(results, ResultsTuple{
			Item:        timestamp,
			Aggregation: int(aggregationValue),
		})
	}

	return results, err
}

// ProcessTopNQuery Run a TopN query based on the query params, the datasource name and the current Druid connection. As a result it returns
// a ResultsTuple with all the data we need to compose the final response in the logic layer
func ProcessTopNQuery(parameters DruidQueryParams, datasourceName string, druidClient *druid.Client) ([]ResultsTuple, error) {
	// Parameters customization (request context -> DruidUtils context)
	table := datasource.NewTable().SetName(datasourceName)
	startDate, endDate := GetStartEndDate(parameters.Year, parameters.Month, parameters.Day)
	interval := intervals.NewInterval().SetInterval(startDate, endDate)
	myIntervals := intervals.NewIntervals().SetIntervals([]*intervals.Interval{interval})

	// Collect all needed Druid filters based on the parameters that haven been passed in the request
	allFilters := getDruidFiltersFromParameters(parameters)

	// Prepare the aggregation (count or sum)
	var countOrSumAggregation []builder.Aggregator
	if parameters.AggregationType == Druid.Aggregated.Count {
		count := aggregation.NewCount().SetName(parameters.AggregationType)
		countOrSumAggregation = []builder.Aggregator{count}
	} else {
		sum := aggregation.NewLongSum().SetFieldName(parameters.AggregationField).SetName(parameters.AggregationType)
		countOrSumAggregation = []builder.Aggregator{sum}
	}

	// Query execution
	scan := query.NewTopN().SetDataSource(table).
		SetIntervals(myIntervals).
		SetMetric(builder.TopNMetric(topnmetric.NewNumeric().SetMetric(parameters.AggregationType))).
		SetThreshold(Druid.TopThreshold).
		SetDimension(builder.Dimension(dimension.NewDefault().SetDimension(parameters.TopDimension))).
		SetFilter(allFilters).
		SetAggregations(countOrSumAggregation).
		SetGranularity(getDruidBuilderGranularity(parameters.Day))

	// Get the query results
	var queryResults interface{}
	_, err := druidClient.Query().Execute(scan, &queryResults)
	if err != nil {
		return []ResultsTuple{}, err
	}

	// In case there is no data for the executed query, an empty ResultsTuple will be returned
	items := queryResults.([]interface{})
	if len(items) == 0 {
		return []ResultsTuple{}, nil
	}

	// If any data has been fetched, preprocess those results before returning them to data layer
	var results []ResultsTuple
	queryItems := getItemsFromTopNQuery(items)
	for _, item := range queryItems {
		count, _ := getFloatValueFromMap(item, parameters.AggregationType)
		userText := getStringValueFromMap(item, parameters.TopDimension)
		results = append(results, ResultsTuple{
			Item:        userText,
			Aggregation: int(count),
		})
	}
	return results, err
}
