package aqsassist

import (
	"os"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
)

// MergeDruidEnvironment takes override values for Druid Username and Password
// supplied in the environment, and merges them with a Config struct.
func MergeDruidEnvironment(config *Config, logger *log.Logger) {

	druidUsername, userOk := os.LookupEnv("DRUID_USERNAME")
	druidPassword, passOk := os.LookupEnv("DRUID_PASSWORD")

	if userOk && !passOk {
		logger.Warning("DRUID_USERNAME env var provided but DRUID_PASSWORD unset, using values from configuration file.")
	}

	if passOk && !userOk {
		logger.Warning("DRUID_PASSWORD env var provided but DRUID_USERNAME unset, using values from configuration file.")
	}

	if !passOk && !userOk {
		logger.Warning("DRUID_USERNAME and DRUID_PASSWORD env vars unset, using values from configuration file.")
	}

	if userOk && passOk {
		config.Druid.Authentication.Username = druidUsername
		config.Druid.Authentication.Password = druidPassword
		logger.Info("Druid login credentials configured from environment")
	}
}

// MergeCassandraEnvironment takes override values for Cassandra Username and Password
// supplied in the environment, and merges them with a Config struct.
func MergeCassandraEnvironment(config *Config, logger *log.Logger) {

	cassandraUsername, userOk := os.LookupEnv("CASSANDRA_USERNAME")
	cassandraPassword, passOk := os.LookupEnv("CASSANDRA_PASSWORD")

	if userOk && !passOk {
		logger.Warning("CASSANDRA_USERNAME env var provided but CASSANDRA_PASSWORD unset, using values from configuration file.")
	}

	if passOk && !userOk {
		logger.Warning("CASSANDRA_PASSWORD env var provided but CASSANDRA_USERNAME unset, using values from configuration file.")
	}

	if !userOk && !passOk {
		logger.Warning("CASSANDRA_USERNAME and CASSANDRA_PASSWORD env vars unset, using values from configuration file.")
	}

	if userOk && passOk {
		config.Cassandra.Authentication.Username = cassandraUsername
		config.Cassandra.Authentication.Password = cassandraPassword
		logger.Info("Cassandra login credentials configured from environment")
	}
}
