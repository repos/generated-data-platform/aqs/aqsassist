module gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist

go 1.17

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v0.0.0-20220322011350-df509f780b5c
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2
	github.com/gocql/gocql v1.6.0
	github.com/grafadruid/go-druid v0.0.6
	github.com/stretchr/testify v1.8.1
	golang.org/x/text v0.13.0
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.8.1
)

require (
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.4 // indirect
	github.com/magefile/mage v1.15.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	github.com/tj/assert v0.0.3 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hhsnopek/etag v0.0.0-20171206181245-aea95f647346
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1
)
