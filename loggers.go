package aqsassist

import (
	"fmt"
	"strings"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
)

// LogMarshalError Log a "unable to marshal" error
func LogMarshalError(logger *log.RequestScopedLogger, errorMessage string) {
	detail := fmt.Sprintf(UnableToMarshallMessage, errorMessage)
	logger.Log(log.ERROR, detail)
}

// LogUnMarshalError Log a "unable to marshal" error
func LogUnMarshalError(logger *log.RequestScopedLogger, errorMessage string) {
	detail := fmt.Sprintf(FailedToUnmarshalDataErrorMessage, errorMessage)
	logger.Log(log.ERROR, detail)
}

func LogDruidDataSourceError(logger *log.RequestScopedLogger, errorMessage string) {
	detail := fmt.Sprintf(DruidDataSourceNotFound, errorMessage)
	logger.Log(log.ERROR, detail)
}

// LogQueryFailed Log a "query failed" error
func LogQueryFailed(logger *log.RequestScopedLogger, errorMessage string) {
	detail := fmt.Sprintf(QueryFailedMessage, errorMessage)
	logger.Log(log.ERROR, detail)
}

// LogQueryingDatabaseError Log a "querying database" error
func LogQueryingDatabaseError(logger *log.RequestScopedLogger, errorMessage string) {
	detail := fmt.Sprintf(ErrorQueryingDatabaseMessage, errorMessage)
	logger.Log(log.ERROR, detail)
}

// LogServiceInitialized Log service initialization message
func LogServiceInitialized(logger *log.Logger, serviceName, version, buildHost, buildDate string) {
	logger.Log(log.INFO, ServiceInitializationMessage, serviceName, version, buildHost, buildDate)
}

// LogCassandraServiceInitialization Log full cassandra-based service initialization message (including database connection)
func LogCassandraServiceInitialization(logger *log.Logger, serviceName, version, buildHost, buildDate string,
	cassandraHosts []string, cassandraPort int, cassandraConsistency, cassandraLocalDC string) {
	LogServiceInitialized(logger, serviceName, version, buildHost, buildDate)
	logger.Log(log.INFO, CassandraConnectingMessage, strings.Join(cassandraHosts, ","), cassandraPort)
	logger.Log(log.DEBUG, CassandraConsistencyLevelMessage, strings.ToLower(cassandraConsistency))
	logger.Log(log.DEBUG, CassandraLocalDatacenterMessage, cassandraLocalDC)
}

func logOnlyCassandraPasswordUnset(logger *log.RequestScopedLogger) {
	logger.Log(log.WARNING, OnlyCassandraPasswordUnsetMessage)
}
