package aqsassist

// No data found messages

const DataNotFoundMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information"
const DataNotFoundCountryMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the country you asked for is not loaded yet. Please check documentation for more information"
const DataNotFoundCategoryMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the category you asked for is not loaded yet. Please check documentation for more information"
const DataNotFoundMediafileMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the media file you asked for is not loaded yet. Please check documentation for more information"
const DataNotFoundYearMonthMessage = "The year and month you used are valid, but we either do not have data for those year and month, or the category scope does not match. Please check documentation for more information"
const DataNotFoundUserNameMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the user name you asked for is not loaded yet. Please check documentation for more information"
const DataNotFoundWikiMessage = "The date(s) you used are valid, but we either do not have data for those date(s), or the wiki you asked for is not loaded yet. Please check documentation for more information"

// Error messages

const ErrorQueryingDatabaseMessage = "error querying database: %s"
const UnableToMarshallMessage = "unable to marshal response object: %s"
const QueryFailedMessage = "query failed: %s"
const ErrorInitializingLoggerMessage = "unable to initialize the logger: %s"
const DruidConnectionErrorMessage = "error connecting to Druid database: %s"
const CassandraConnectionErrorMessage = "failed to create Cassandra session: %s"
const SwaggerFileLoadingErrorMessage = "failed to read swagger file: %s"
const FailedToCreateHandlerPathErrorMessage = "failed to load handler file path"
const FailedToUnmarshalDataErrorMessage = "unable to unmarshal returned data: %s"
const FailedToCreateRequestErrorMessage = "failed to create test request: %v"
const UnsupportedLogLevelMessage = "Unsupported log level: %s"
const UnsupportedConsistencyErrorMessage = "Unsupported consistency level: %s"
const ProblemAccessingDataMessage = "problem accessing the data"
const UnabletoMarshalMessageJson = "could not marshal problem to json: %s"
const QueryMoreThanOneMessageJson = "Query matched more than one result"

// Information messages

const DruidConnectedMessage = "connected to Druid version: %s"
const CassandraConnectingMessage = "connecting to Cassandra database(s): %s (port %d)"
const CassandraConsistencyLevelMessage = "Cassandra: configured for consistency level '%s'"
const CassandraLocalDatacenterMessage = "Cassandra: configured for local datacenter '%s'"
const ServiceInitializationMessage = "initializing service %s (Go version: %s, Build host: %s, Item: %s"
const DruidDataSourceNotFound = "could not get MediaWiki history reduced druid datasource from DynamicConfig: %s"

const OnlyDruidPasswordUnsetMessage = "DRUID_USERNAME env var provided but DRUID_PASSWORD unset, using values from configuration file"
const OnlyDruidUsernameUnsetMessage = "DRUID_PASSWORD env var provided but DRUID_USERNAME unset, using values from configuration file"
const BothDruidCredentialsUnsetMessage = "DRUID_USERNAME and DRUID_PASSWORD env vars unset, using values from configuration file"
const DruidCredentialsSetFromEnvironmentMessage = "Druid login credentials configured from environment"

const OnlyCassandraPasswordUnsetMessage = "CASSANDRA_USERNAME env var provided but CASSANDRA_PASSWORD unset, using values from configuration file"
const OnlyCassandraUsernameUnsetMessage = "CASSANDRA_PASSWORD env var provided but CASSANDRA_USERNAME unset, using values from configuration file"
const BothCassandraCredentialsUnsetMessage = "CASSANDRA_USERNAME and CASSANDRA_PASSWORD env vars unset, using values from configuration file"
const CassandraCredentialsSetFromEnvironmentMessage = "Cassandra login credentials configured from environment"

const ConfigurationPathMessage = "path to the configuration file"

// Validation messages

const InvalidAccessMessage = "access should be equal to one of the allowed values: [all-access, desktop, mobile-app, mobile-web]"
const InvalidAccessSiteMessage = "access-site should be equal to one of the allowed values: [all-sites, desktop-site, mobile-site]"
const InvalidPageAgentMessage = "agent should be equal to one of the allowed values: [all-agents, automated, spider, user]"
const InvalidEditorTypeMessage = "editor-type should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user]"
const InvalidPageTypeMessage = "page-type should be equal to one of the allowed values: [all-page-types, content, non-content]"
const InvalidFullActivityLevelMessage = "activity-level should be equal to one of the allowed values: [all-activity-levels, 1..4-edits, 5..24-edits, 25..99-edits, 100..-edits]"
const InvalidGranularityNoHourlyMessage = "granularity should be equal to one of the allowed values: [daily, monthly]"
const InvalidGranularityIncludingHourlyMessage = "granularity should be equal to one of the allowed values: [hourly, daily, monthly]"
const InvalidMediaTypeMessage = "media-type should be equal to one of the allowed values: [all-media-types, image, video, audio, document, other]"
const InvalidGeoActivityLevelMessage = "activity-level should be equal to one of the allowed values: [5..99-edits, 100..-edits]"
const YearMonthDayInvalidDateMessage = "given year/month/day is invalid date"
const YearMonthInvalidDateMessage = "given year/month is invalid date"
const InvalidDateEndBeforeStart = "start timestamp should be before the end timestamp"
const InvalidHHStartDateMessage = "start timestamp is invalid, must be a valid date in YYYYMMDDHH format"
const InvalidDDStartDateMessage = "start timestamp is invalid, must be a valid date in YYYYMMDD format"
const InvalidHHEndDateMessage = "end timestamp is invalid, must be a valid date in YYYYMMDDHH format"
const InvalidDDEndDateMessage = "end timestamp is invalid, must be a valid date in YYYYMMDD format"
const NoFullMonthsBetweenDatesMessage = "no full months found in specified date range"
const AllNotAcceptedMessage = "all-...` project values are not accepted for this metric"
const InvalidProjectCharactersMessage = "the parameter `project` contains invalid characters"
const InvalidCharacterReferersMessage = "the parameter `referer` contains invalid characters"
const InvalidRouteMessage = "invalid route"
const InvalidMediaAgentMessage = "agent should be equal to one of the allowed values: [all-agents, user, spider]"
const InvalidEditTypeMessage = "edit values should be equal to one of the allowed values: [create,update,all-edit-types]"
const InvalidCategoryScopeMessage = "category scope should be equal to one of the allowed values: [shallow,deep]"
const InvalidWikiCharactersMessage = "the parameter `wiki` contains invalid characters"
const InvalidOriginalWikiCharactersMessage = "the parameter `original-wiki` contains invalid characters"
const InvalidReportingWikiCharactersMessage = "the parameter `reporting-wiki` contains invalid characters"
const InvalidPagetitleCharactersMessage = "the parameter `page-title` contains invalid characters"

// Common Paths

const SwaggerDocPath = "/docs/swagger.json"
