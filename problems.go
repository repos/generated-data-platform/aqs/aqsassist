package aqsassist

import (
	"fmt"
	"net/http"
	"strings"

	"schneider.vip/problem"
)

// CreateProblem Create a problem given a status, detail and uri
func CreateProblem(status int, detail string, uri string) *problem.Problem {
	return problem.New(
		problem.Type("about:blank"),
		problem.Title(http.StatusText(status)),
		problem.Custom("method", strings.ToLower(http.MethodGet)),
		problem.Status(status),
		problem.Custom("detail", detail),
		problem.Custom("uri", uri),
	)
}

// CreateDataNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundMessage as the response detail
func CreateDataNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundMessage, reqUrl)
}

// CreateCategoryNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundCategoryMessage as the response detail
func CreateCategoryNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundCategoryMessage, reqUrl)
}

// CreateMediafileNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundMediafileMessage as the response detail
func CreateMediafileNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundMediafileMessage, reqUrl)
}

// CreateYearMonthNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundYearMonthMessage as the response detail
func CreateYearMonthNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundYearMonthMessage, reqUrl)
}

// CreateUserNameNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundUserNameMessage as the response detail
func CreateUserNameNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundUserNameMessage, reqUrl)
}

// CreateCountryNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundCountryMessage as the response detail
func CreateCountryNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundCountryMessage, reqUrl)
}

// CreateInvalidRouteProblem Create a "no data found" problem with a 404 Not Found status code and InvalidRouteMessage as the response detail
func CreateInvalidRouteProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, InvalidRouteMessage, reqUrl)
}

// Validation functions

// CreateBadRequestProblem Create a Bad Request problem with the message and reqUrl provided
func CreateBadRequestProblem(message, reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusBadRequest, message, reqUrl)
}

// CreateInvalidAccessProblem Create an invalid access problem with a 400 Bad Request status code and InvalidAccessMessage as the response detail
func CreateInvalidAccessProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidAccessMessage, reqUrl)
}

// CreateInvalidAccessSiteProblem Create an invalid access site problem with a 400 Bad Request status code and InvalidAccessSiteMessage as the response detail
func CreateInvalidAccessSiteProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidAccessSiteMessage, reqUrl)
}

// CreateInvalidAgentProblem Create an invalid agent problem with a 400 Bad Request status code and InvalidMediaAgentMessage as the response detail
func CreateInvalidAgentProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidMediaAgentMessage, reqUrl)
}

// CreateInvalidEditorTypeProblem Create an invalid editor type problem with a 400 Bad Request status code and InvalidEditorTypeMessage as the response detail
func CreateInvalidEditorTypeProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidEditorTypeMessage, reqUrl)
}

// CreateInvalidPageTypeProblem Create an invalid page type problem with a 400 Bad Request status code and InvalidPageTypeMessage as the response detail
func CreateInvalidPageTypeProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidPageTypeMessage, reqUrl)
}

// CreateInvalidActivityLevelProblem Create an invalid activity level problem with a 400 Bad Request status code and InvalidFullActivityLevelMessage as the response detail
func CreateInvalidActivityLevelProblem(reqUrl string) *problem.Problem {
	return CreateBadRequestProblem(InvalidFullActivityLevelMessage, reqUrl)
}

// Error functions

// CreateInternalServerErrorProblem Create an Internal Server Error problem with the message and reqUrl provided
func CreateInternalServerErrorProblem(message, reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusInternalServerError, message, reqUrl)
}

// CreateErrorQueryingDatabaseProblem Create an "error querying database" problem with a 500 Internal Server Error status code and ErrorQueryingDatabaseMessage as the response detail
func CreateErrorQueryingDatabaseProblem(errorMessage, reqUrl string) *problem.Problem {
	detail := fmt.Sprintf(ErrorQueryingDatabaseMessage, errorMessage)
	return CreateInternalServerErrorProblem(detail, reqUrl)
}

// CreateUnableToMarshalProblem Create an "unable to marshal" level problem with a 500 Internal Server Error status code and UnableToMarshallMessage as the response detail
func CreateUnableToMarshalProblem(errorMessage, reqUrl string) *problem.Problem {
	detail := fmt.Sprintf(UnableToMarshallMessage, errorMessage)
	return CreateInternalServerErrorProblem(detail, reqUrl)
}

// CreateQueryFailedProblem Create a "query failed" problem with a 500 Internal Server Error status code and QueryFailedMessage as the response detail
func CreateQueryFailedProblem(errorMessage, reqUrl string) *problem.Problem {
	detail := fmt.Sprintf(QueryFailedMessage, errorMessage)
	return CreateInternalServerErrorProblem(detail, reqUrl)
}

// CreateWikiNotFoundProblem Create a "no data found" problem with a 404 Not Found status code and DataNotFoundWikiMessage as the response detail
func CreateWikiNotFoundProblem(reqUrl string) *problem.Problem {
	return CreateProblem(http.StatusNotFound, DataNotFoundWikiMessage, reqUrl)
}
