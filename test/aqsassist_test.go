package test

import (
	"net/http/httptest"
	"testing"
	"time"

	"strings"

	"github.com/stretchr/testify/assert"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
)

func TestValidateTimestamp(t *testing.T) {
	timestamp := "20220101"

	validatedTimestamp, err := aqsassist.ValidateTimestamp(timestamp)

	if len(validatedTimestamp) != 10 || err != nil {
		t.Fatalf(`Got '%s,', want 2022010100`, validatedTimestamp)
	}
}

func TestInvalidTimestamp(t *testing.T) {
	timestamp := ""

	_, err := aqsassist.ValidateTimestamp(timestamp)

	if err == nil {
		t.Fatalf(`Got %s, want parsing error`, err)
	}
}

func TestTrimProjectDomain(t *testing.T) {
	domain := "www.es.wikipedia.org"

	trimmedDomain := aqsassist.TrimProjectDomain(domain)

	if trimmedDomain != "es.wikipedia" {
		t.Fatalf(`Got %s, want es.wikipedia`, trimmedDomain)
	}
}

func TestValidateProject(t *testing.T) {
	invalidProject := "www.en*wikipedia.org"
	anotherInvalidProject := "^es.wikipedia.org"
	validProject := "en.wikipedia.org"
	anotherValidProject := "www.en.wiktionary"

	isValid, project := aqsassist.ValidateProject(invalidProject)
	assert.False(t, isValid)
	assert.Equal(t, "en*wikipedia", project)

	isValid, project = aqsassist.ValidateProject(anotherInvalidProject)
	assert.False(t, isValid)
	assert.Equal(t, "^es.wikipedia", project)

	isValid, project = aqsassist.ValidateProject(validProject)
	assert.True(t, isValid)
	assert.Equal(t, "en.wikipedia", project)

	isValid, project = aqsassist.ValidateProject(anotherValidProject)
	assert.True(t, isValid)
	assert.Equal(t, "en.wiktionary", project)
}

func TestValidateReferer(t *testing.T) {
	inexistentProjectAsReferer := "internals"
	invalidCharactersReferer := "^search"
	validReferer := "INTernal"
	validProjectAsReferer := "en.wikipedia.org"

	isValid, referer := aqsassist.ValidateReferer(inexistentProjectAsReferer)
	assert.True(t, isValid)
	assert.Equal(t, "internals", referer)

	isValid, referer = aqsassist.ValidateReferer(invalidCharactersReferer)
	assert.False(t, isValid)
	assert.Equal(t, "^search", referer)

	isValid, referer = aqsassist.ValidateReferer(validReferer)
	assert.True(t, isValid)
	assert.Equal(t, "internal", referer)

	isValid, referer = aqsassist.ValidateReferer(validProjectAsReferer)
	assert.True(t, isValid)
	assert.Equal(t, "en.wikipedia", referer)
}

func TestNormalizePageTitle(t *testing.T) {
	pageTitleToNormalize := "Museo del Prado"

	assert.Equal(t, "Museo_del_Prado", aqsassist.NormalizePageTitle(pageTitleToNormalize))
}

func TestFilterAgent(t *testing.T) {
	agent := "all-agents"

	total, _ := aqsassist.FilterAgent(agent, 1, 1)

	if total != 2 {
		t.Fatalf(`Got %d, want 2`, total)
	}
}

func TestBadFilterAgent(t *testing.T) {
	agent := "all"

	total, err := aqsassist.FilterAgent(agent, 1, 1)

	if total != 0 || err == nil {
		t.Fatalf(`Got %d, want to throw an error`, total)
	}
}

// ValidateDuration for monthly granularity
func TestFullMonth(t *testing.T) {
	start := "2022010100"
	end := "2022013100"
	granularity := "monthly"

	_, _, err := aqsassist.ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf(`Got %s, want nil`, err)
	}

}

func TestLessThanOneMonth(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "monthly"

	s, e, err := aqsassist.ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

func TestLessThanTwoMonths(t *testing.T) {
	//Duration longer than a month,
	//no full months in range
	start := "2022010200"
	end := "2022022700"
	granularity := "monthly"

	s, e, err := aqsassist.ValidateDuration(start, end, granularity)

	if err == nil {
		t.Fatalf("Got %s, %s, want to throw an err", s, e)
	}
}

// ValidateDuration for daily granularity
func TestDaily(t *testing.T) {
	//Duration shorter than a month,
	//no full months in range
	start := "2022010100"
	end := "2022010200"
	granularity := "daily"

	_, _, err := aqsassist.ValidateDuration(start, end, granularity)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}

func TestStartBeforeEnd(t *testing.T) {
	start := "2021020100"
	end := "2021010100"

	err := aqsassist.StartBeforeEnd(start, end)

	if err == nil {
		t.Fatalf("Err %s, want error", err)
	}

	err = aqsassist.StartBeforeEnd(start, start)

	if err != nil {
		t.Fatalf("Got %s, want nil", err)
	}
}

func TestSecureTestURL(t *testing.T) {
	apiTestUrl := aqsassist.TestURL(aqsassist.TestURLParam{
		Fallback: "http://localhost:8080",
	})

	url := apiTestUrl("test-endpoint")

	if url != "http://localhost:8080/test-endpoint" {
		t.Error("Urls are not the same")
	}
}

func TestSetSecurityHeaders(t *testing.T) {
	rec := httptest.NewRecorder()

	aqsassist.SetSecurityHeaders(rec, rec.Body.Bytes())
	for headerName, headerValue := range aqsassist.HeadersToAdd {
		if rec.Header().Get(headerName) != headerValue {
			t.Error("Header value " + headerName + " incorrect or missing ")
		}
	}
	etagHeaderVal := rec.Header().Get("etag")
	if len(strings.TrimSpace(etagHeaderVal)) == 0 {
		t.Error("Header value etag " + " incorrect or missing ")

	}

}

func TestGetTime(t *testing.T) {
	year := "2023"
	month := "03"
	day := "23"

	expectedDate := time.Date(2023, time.March, 23, 0, 0, 0, 0, time.UTC)
	realDate := aqsassist.GetTime(year, month, day)
	assert.Equal(t, expectedDate, realDate)
}

func TestGetMarshallTextTime(t *testing.T) {
	year := "2023"
	month := "03"
	day := "23"

	expectedDate, _ := time.Date(2023, time.March, 23, 0, 0, 0, 0, time.UTC).MarshalText()
	expectedString := string(expectedDate)
	realString := aqsassist.GetMarshallTextTime(year, month, day)
	assert.Equal(t, expectedString, realString)
}

func TestGetStartEndDate(t *testing.T) {
	year := "2023"
	month := "03"
	day := "all-days"

	expectedStartDate := time.Date(2023, time.March, 01, 0, 0, 0, 0, time.UTC)
	expectedEndDate := time.Date(2023, time.April, 01, 00, 0, 0, 0, time.UTC)
	realStartDate, realEndDate := aqsassist.GetStartEndDate(year, month, day)
	assert.Equal(t, expectedStartDate, realStartDate)
	assert.Equal(t, expectedEndDate, realEndDate)
}

func TestIsYear(t *testing.T) {
	validYear := "2023"
	invalidYear := "103"

	validYearResult := aqsassist.IsYear(validYear)
	invalidYearResult := aqsassist.IsYear(invalidYear)
	assert.Equal(t, true, validYearResult)
	assert.Equal(t, false, invalidYearResult)
}

func TestIsMonth(t *testing.T) {
	validMonth := "03"
	invalidMonth := "3"

	validMonthResult := aqsassist.IsMonth(validMonth)
	invalidMonthResult := aqsassist.IsMonth(invalidMonth)
	assert.Equal(t, true, validMonthResult)
	assert.Equal(t, false, invalidMonthResult)
}

func TestIsDay(t *testing.T) {
	validDay := "all-days"
	invalidDay := "29"

	validDayResult := aqsassist.IsDay("2023", "02", validDay, true)
	invalidDayResult := aqsassist.IsDay("2023", "02", invalidDay, false)
	assert.Equal(t, true, validDayResult)
	assert.Equal(t, false, invalidDayResult)
}

func TestGetDaysIn(t *testing.T) {
	month := "02"
	year := "2023"

	expectedValue := 28
	realValue := aqsassist.GetDaysIn(year, month)
	assert.Equal(t, expectedValue, realValue)
}

func TestValidateFullMonthsBetween(t *testing.T) {
	// ok: one full month is specified
	start := "2023020100"
	end := "2023030100"
	expectedStart := "2023020100"
	expectedEnd := "2023030100"
	actualStart, actualEnd, err := aqsassist.ValidateFullMonthsBetween(start, end, true)
	assert.Empty(t, err)
	assert.Equal(t, expectedStart, actualStart)
	assert.Equal(t, expectedEnd, actualEnd)

	// ok: one full month is specified
	start = "2023020100"
	end = "2023022800"
	expectedStart = "2023020100"
	expectedEnd = "2023022800"
	actualStart, actualEnd, err = aqsassist.ValidateFullMonthsBetween(start, end, false)
	assert.Empty(t, err)
	assert.Equal(t, expectedStart, actualStart)
	assert.Equal(t, expectedEnd, actualEnd)

	// ok: but only one month is fully specified
	start = "2023020200"
	end = "2023043000"
	expectedStart = "2023030100"
	expectedEnd = "2023043000"
	actualStart, actualEnd, err = aqsassist.ValidateFullMonthsBetween(start, end, false)
	assert.Empty(t, err)
	assert.Equal(t, expectedStart, actualStart)
	assert.Equal(t, expectedEnd, actualEnd)

	// ok: but only one month is fully specified
	start = "2023020100"
	end = "2023042900"
	expectedStart = "2023020100"
	expectedEnd = "2023033100"
	actualStart, actualEnd, err = aqsassist.ValidateFullMonthsBetween(start, end, false)
	assert.Empty(t, err)
	assert.Equal(t, expectedStart, actualStart)
	assert.Equal(t, expectedEnd, actualEnd)

	// error: No full month specified
	start = "2023030200"
	end = "2023033100"
	_, _, err = aqsassist.ValidateFullMonthsBetween(start, end, false)
	assert.NotEmpty(t, err)
}

func TestIsEditorType(t *testing.T) {
	validEditorType := "user"
	invalidEditorType := "invalid-user"

	validResult := aqsassist.IsEditorType(validEditorType)
	invalidResult := aqsassist.IsEditorType(invalidEditorType)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsPageType(t *testing.T) {
	validPageType := "non-content"
	invalidPageType := "important-content"

	validResult := aqsassist.IsPageType(validPageType)
	invalidResult := aqsassist.IsPageType(invalidPageType)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsActivityLevel(t *testing.T) {
	validActivityLevel := "25..99-edits"
	invalidActivityLevel := "33..55-edits"

	validResult := aqsassist.IsActivityLevel(validActivityLevel)
	invalidResult := aqsassist.IsActivityLevel(invalidActivityLevel)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsGranularity(t *testing.T) {
	validGranularity := "monthly"
	invalidGranularity := "yearly"

	validResult := aqsassist.IsGranularity(validGranularity)
	invalidResult := aqsassist.IsGranularity(invalidGranularity)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsMediaType(t *testing.T) {
	validMediaType := "document"
	invalidMediaType := "all-referers"

	validResult := aqsassist.IsMediaType(validMediaType)
	invalidResult := aqsassist.IsMediaType(invalidMediaType)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsAgent(t *testing.T) {
	validAgent := "spider"
	invalidAgent := "browser"

	validResult := aqsassist.IsAgent(validAgent)
	invalidResult := aqsassist.IsAgent(invalidAgent)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestIsAccessSite(t *testing.T) {
	validSite := "all-sites"
	invalidSite := "invalid-sites"

	validResult := aqsassist.IsAccessSite(validSite)
	invalidResult := aqsassist.IsAccessSite(invalidSite)
	assert.Equal(t, true, validResult)
	assert.Equal(t, false, invalidResult)
}

func TestValidation_InvalidYearMonth(t *testing.T) {
	var year, month string
	var err error
	year, month, err = aqsassist.ValidateYearMonth("2021", "13")
	assert.Equal(t, aqsassist.YearMonthInvalidDateMessage, err.Error())
	assert.Empty(t, year)
	assert.Empty(t, month)

	year, month, err = aqsassist.ValidateYearMonth("202", "09")
	assert.Equal(t, aqsassist.YearMonthInvalidDateMessage, err.Error())
	assert.Empty(t, year)
	assert.Empty(t, month)

	year, month, err = aqsassist.ValidateYearMonth("2", "87")
	assert.Equal(t, aqsassist.YearMonthInvalidDateMessage, err.Error())
	assert.Empty(t, year)
	assert.Empty(t, month)

	year, month, err = aqsassist.ValidateYearMonth("2002", "07")
	assert.Nil(t, err)
	assert.NotEmpty(t, year)
	assert.NotEmpty(t, month)

}
