package test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	yaml "gopkg.in/yaml.v2"
)

func TestNewConfig(t *testing.T) {
	config := aqsassist.Config{
		ServiceName:    "some-service",
		Address:        "somehost",
		Port:           8080,
		LogLevel:       "info",
		ContextTimeout: 40,
		Cassandra: aqsassist.CassandraConfig{
			Port:        9042,
			Consistency: "quorum",
			Hosts:       []string{"anotherhost"},
			ConfigTable: "aqs.config",
		},
		Druid: aqsassist.DruidConfig{
			Host: "http://yetanotherhost",
			Port: "8082",
		},
	}
	// Format config as yaml byte array to test NewConfig.
	config_data, _ := yaml.Marshal(config)
	newConfig, err := aqsassist.NewConfig(config_data)
	assert.Equal(t, config, *newConfig)
	assert.Equal(t, nil, err)
}

func TestValidate(t *testing.T) {
	config := aqsassist.Config{
		ServiceName:    "some-service",
		Address:        "somehost",
		Port:           8080,
		LogLevel:       "info",
		ContextTimeout: 40,
		Cassandra: aqsassist.CassandraConfig{
			Port:        9042,
			Consistency: "quorum",
			Hosts:       []string{"anotherhost"},
			ConfigTable: "aqs.config",
		},
		Druid: aqsassist.DruidConfig{
			Host: "http://yetanotherhost",
			Port: "8082",
		},
	}
	_, err := aqsassist.Validate(&config)
	assert.Equal(t, nil, err)
}

func TestValidateCassandraConsistency(t *testing.T) {
	supportedConsistencies := []string{"any", "one", "two", "three", "quorum", "all", "localquorum", "eachquorum", "localone"}
	for _, consistency := range supportedConsistencies {
		config := aqsassist.CassandraConfig{Consistency: consistency}
		err := aqsassist.ValidateCassandraConsistency(config)
		assert.Equal(t, nil, err)
	}
	invalid_config := aqsassist.CassandraConfig{Consistency: "INVALID_CONSISTENCY"}
	err := aqsassist.ValidateCassandraConsistency(invalid_config)
	assert.Equal(t, fmt.Errorf("Unsupported consistency level: INVALID_CONSISTENCY"), err)
}

func TestValidateLogLevel(t *testing.T) {
	supportedLogLevels := []string{"DEBUG", "INFO", "WARNING", "ERROR", "FATAL"}
	for _, logLevel := range supportedLogLevels {
		err := aqsassist.ValidateLogLevel(logLevel)
		assert.Equal(t, nil, err)
	}
	err := aqsassist.ValidateLogLevel("INVALID_LOG_LEVEL")
	assert.Equal(t, fmt.Errorf("Unsupported log level: INVALID_LOG_LEVEL"), err)
}

type MockConfigReader struct {
	mock.Mock
}

func (m *MockConfigReader) Get(property string) (string, error) {
	args := m.Called(property)
	return args.String(0), args.Error(1)
}

func TestNewDynamicConfig(t *testing.T) {
	reader := new(MockConfigReader)
	dc, err := aqsassist.NewDynamicConfig(reader, 3600)
	assert.Equal(t, reader, dc.Reader)
	assert.Equal(t, int64(3600), dc.TTL)
	assert.Equal(t, map[string]aqsassist.ValueAndExpiration{}, dc.Cache)
	assert.Equal(t, nil, err)
}

func TestGetString(t *testing.T) {
	// Create the DynamicConfigReader and the DynamciConfig objects.
	// Use 1 second cache TTL, so we can test the cache eviction case.
	reader := new(MockConfigReader)
	dc, _ := aqsassist.NewDynamicConfig(reader, 1)

	// Test a first call to DynamicConfig.GetString.
	// The property value is not cached, so we expect reader.Get to be called.
	mockCall := reader.On("Get", "property").Return("value", nil)
	value, err := dc.GetString("property")
	reader.AssertExpectations(t)
	reader.AssertNumberOfCalls(t, "Get", 1)
	assert.Equal(t, "value", value)
	assert.Equal(t, nil, err)
	mockCall.Unset()

	// Test a second call to GetString.
	// The property value is cached, so we expect no calls to reader.Get.
	value, err = dc.GetString("property")
	reader.AssertExpectations(t)
	reader.AssertNumberOfCalls(t, "Get", 1)
	assert.Equal(t, "value", value)
	assert.Equal(t, nil, err)

	// Test cache eviction. Wait after TTL has passed.
	// The property value is cached, but it expired.
	// So we should see a call to reader.Get again.
	time.Sleep(time.Second)
	mockCall = reader.On("Get", "property").Return("value", nil)
	value, err = dc.GetString("property")
	reader.AssertExpectations(t)
	reader.AssertNumberOfCalls(t, "Get", 2)
	assert.Equal(t, "value", value)
	assert.Equal(t, nil, err)
	mockCall.Unset()

	// Test a fourth call to GetString, after cache eviction.
	// The property value is cached again, so we expect no calls to reader.Get.
	value, err = dc.GetString("property")
	reader.AssertExpectations(t)
	reader.AssertNumberOfCalls(t, "Get", 2)
	assert.Equal(t, "value", value)
	assert.Equal(t, nil, err)

	// Test a failed call to GetString.
	// The property does not exist, reader.Get should return an error.
	mockCall = reader.On("Get", "invalid").Return("", fmt.Errorf("Error"))
	value, err = dc.GetString("invalid")
	reader.AssertExpectations(t)
	reader.AssertNumberOfCalls(t, "Get", 3)
	assert.Equal(t, "", value)
	assert.Equal(t, fmt.Errorf("Error"), err)
	mockCall.Unset()
}
