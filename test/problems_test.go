package test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqsassist"
	"schneider.vip/problem"
)

type Problem struct {
	Title  string
	Status int
	Detail string
}

func TestCreateProblem(t *testing.T) {
	str := "error message"
	uri := "https://example.com"

	prob := aqsassist.CreateProblem(http.StatusNotFound, str, uri)

	expectedProblem := problem.New(
		problem.Type("about:blank"),
		problem.Title(http.StatusText(http.StatusNotFound)),
		problem.Custom("method", strings.ToLower(http.MethodGet)),
		problem.Status(http.StatusNotFound),
		problem.Custom("detail", str),
		problem.Custom("uri", uri),
	)

	require.Equal(t, prob, expectedProblem)

	_, err := json.Marshal(prob)
	if err != nil {
		t.Fatalf(aqsassist.UnabletoMarshalMessageJson, err)
	}
}

func TestCreateDataNotFoundProblem(t *testing.T) {
	prob := aqsassist.CreateDataNotFoundProblem("http://test")

	var realProblem Problem
	err := json.Unmarshal(prob.JSON(), &realProblem)
	if err != nil {
		t.Fatalf(aqsassist.UnabletoMarshalMessageJson, err)
	}

	require.Equal(t, aqsassist.DataNotFoundMessage, realProblem.Detail)
	require.Equal(t, http.StatusNotFound, realProblem.Status)
}

func TestCreateInvalidAccessProblem(t *testing.T) {
	prob := aqsassist.CreateInvalidAccessProblem("http://test")

	var realProblem Problem
	err := json.Unmarshal(prob.JSON(), &realProblem)
	if err != nil {
		t.Fatalf(aqsassist.UnabletoMarshalMessageJson, err)
	}

	require.Equal(t, aqsassist.InvalidAccessMessage, realProblem.Detail)
	require.Equal(t, http.StatusBadRequest, realProblem.Status)
}

func TestCreateErrorQueryingDatabaseProblem(t *testing.T) {
	errorMessage := "specific query error message"
	prob := aqsassist.CreateErrorQueryingDatabaseProblem(errorMessage, "http://test")

	var realProblem Problem
	err := json.Unmarshal(prob.JSON(), &realProblem)
	if err != nil {
		t.Fatalf(aqsassist.UnabletoMarshalMessageJson, err)
	}

	fullExpectedErrorMessage := fmt.Sprintf(aqsassist.ErrorQueryingDatabaseMessage, errorMessage)

	require.Equal(t, fullExpectedErrorMessage, realProblem.Detail)
	require.Equal(t, http.StatusInternalServerError, realProblem.Status)
}
