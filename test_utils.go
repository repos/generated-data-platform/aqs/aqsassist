package aqsassist

import (
	"fmt"
	"os"
	"strings"
)

type TestURLParam struct {
	Fallback string
}

func TestURL(input TestURLParam) func(suffix string) string {
	return func(suffix string) string {
		res := os.Getenv("API_URL")
		if res == "" {
			return fmt.Sprintf("%s/%s", input.Fallback, suffix)
		}
		return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
	}
}
